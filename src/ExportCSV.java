import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;

public class ExportCSV {
	public ExportCSV(ArrayList<Service> listServices) {
		final String pathName = "finished_services.csv";
		try (PrintWriter writer = new PrintWriter(new File(pathName))) {
			StringBuilder sb = new StringBuilder();

			for (Service listService : listServices) {
				sb.append("Name").append(",").append(listService.getName()).append("\n");
				sb.append("Beneficiary").append(",").append(listService.getBeneficiary().getName()).append("\n");
				sb.append("Number of members required").append(",").append(listService.getNbMembersRequired()).append("\n");
				sb.append("Skill required").append(",").append(listService.getSkillRequired()).append("\n");
				sb.append("Hourly cost").append(",").append(listService.getHourlyCost()).append("\n");
				sb.append("List Workers").append(",");
				for (int i = 0 ; i < listService.getListWorkers().size() ; i++) {
					sb.append(listService.getListWorkers().get(i).getName()).append(",");
				}
			}

			writer.write(sb.toString());

			System.out.println("There is " + listServices.size() + " finished services which have been archieved in : " + pathName);

		} catch (FileNotFoundException e) {
			System.out.println(e.getMessage());
		}
	}
}