import java.util.ArrayList;

public class Member {
	private String name;
	private int sumTokens;
	private ArrayList<Skill> listSkills;

	/**
	 * Constructor
	 * @param name name of the member
	 * @param sumTokens starting sum of tokens
	 */
	public Member(String name, int sumTokens) {
		this.name = name;
		this.sumTokens = sumTokens;
		listSkills = new ArrayList<>();
	}

	/**
	 * Creates a new network where the creative member will become the first administrator
	 * @param name name of the new network
	 * @return network created
	 */
	public Network createNetwork(String name) {
		return new Network(name, this);
	}

	/**
	 * Permits to leave a network if you are member or if you are admin and there is another admin in the network
	 * @param network network to leave
	 */
	public void leaveNetwork(Network network) throws IsInNetworkException {
		if (isInNetwork(network)) {
			network.getListMembers().remove(this);
			network.getListAdmins().remove(this);
		} else {
			throw new IsInNetworkException(this, new Throwable().getStackTrace()[0].getMethodName(), network);
		}
	}

	/**
	 * Add skill to the list skills member
	 * @param skill skill to add
	 */
	public void addSkill(Skill skill) throws SkillException {
		if (!listSkills.contains(skill)) {
			listSkills.add(skill);
		} else {
			throw new SkillException(this, new Throwable().getStackTrace()[0].getMethodName(), "you already added this skill.");
		}
	}

	/**
	 * Remove skill from the list skills member
	 * @param skill skill to remove
	 */
	public void removeSkill(Skill skill) throws SkillException {
		if (listSkills.contains(skill)) {
			listSkills.remove(skill);
		} else {
			throw new SkillException(this, new Throwable().getStackTrace()[0].getMethodName(), "you can't remove a skill you don't have.");
		}
	}

	/**
	 * Checks if the member is in the network
	 * @param network network to check
	 * @return true if is in the network, false if not
	 */
	public boolean isInNetwork(Network network) {
		return (network.getListMembers().contains(this) || network.getListAdmins().contains(this));
	}

	/**
	 * Checks if the member has the rights to execute an administrator statement
	 * @param network network to check
	 * @return true if the member is admin of the network, false if not
	 */
	private boolean isAdmin(Network network) {
		return network.getListAdmins().contains(this);
	}

	/**
	 * Checks if the service exists in the network
	 * @param network network where heck
	 * @param service service to check
	 * @return true if the service exists in the network, false if not
	 */
	private boolean isServiceExist(Network network, Service service) {
		return network.getListServices().contains(service);
	}

	/**
	 * Permits to create a request for a service on a network
	 * @param network network where create the service
	 * @param name service's name
	 * @param hourlyCost cost of the service by hour
	 * @param nbMembersRequired number of members of the network required to complete the task
	 * @param skillRequired skill required to complete the task
	 * @return service created
	 */
	public Service createService(Network network, String name, int hourlyCost, int nbMembersRequired, Skill skillRequired) throws IsInNetworkException {
		if (isInNetwork(network)) {
			Service service = new Service(name, this, hourlyCost, nbMembersRequired, skillRequired);
			network.addService(service);
			return service;
		} else {
			throw new IsInNetworkException(this, new Throwable().getStackTrace()[0].getMethodName(), network);
		}
	}

	/**
	 * Permits to remove a service from a network if the member is the creator of the service or an administrator of the network
	 * @param network network where check
	 * @param service service to remove
	 * @throws IsInNetworkException if the member is not in the network, throw exception
	 * @throws IsServiceExistException if the service doesn't exist, throw exception
	 */
	public void removeService(Network network, Service service) throws IsInNetworkException, IsServiceExistException {
		int index = network.getListServices().indexOf(service);
		if (isInNetwork(network)) {
			if (isServiceExist(network, service)) {
				if (network.getListServices().get(index).getBeneficiary() == this || network.getListAdmins().contains(this)) {
					network.getListServices().remove(service);
				}
			} else {
				throw new IsServiceExistException(this, new Throwable().getStackTrace()[0].getMethodName(), service, network);
			}
		} else {
			throw new IsInNetworkException(this, new Throwable().getStackTrace()[0].getMethodName(), network);
		}
	}

	/**
	 * Permits to join a service
	 * @param network network where joining the service
	 * @param service service to join
	 * @throws IsInNetworkException if the member is not in the network, throw exception
	 * @throws IsServiceExistException if the service doesn't exist, throw exception
	 * @throws SkillException if the member doesn't have the skill required, throw exception
	 */
	public void joinService(Network network, Service service) throws IsInNetworkException, IsServiceExistException, SkillException {
		if (isInNetwork(network)) {
			network.joinService(this, service);
		} else {
			throw new IsInNetworkException(this, new Throwable().getStackTrace()[0].getMethodName(), network);
		}
	}

	/**
	 * Permits to change the member's social class if the statement's user is administrator
	 * @param network network where change the member's social class
	 * @param member member to change his social class
	 * @param socialClass new social class
	 * @throws IsAdminException if the member is not admin, throw exception
	 */
	public void changeSocialClass(Network network, Member member, SocialClass socialClass) throws IsAdminException {
		if (isAdmin(network)) {
			network.changeSocialClass(member, socialClass);
		} else {
			throw new IsAdminException(this, new Throwable().getStackTrace()[0].getMethodName(), network);
		}
	}

	/**
	 * Adds a member to a network where the statement's user is administrator
	 * @param network network's name where add a member
	 * @param member member to add
	 * @throws IsAdminException if the member is not admin, throw exception
	 */
	public void addMember(Network network, Member member) throws IsAdminException {
		if (isAdmin(network)) {
			network.getListMembers().add(member);
			network.getListSocialClassByMembers().put(member, SocialClass.REGULAR);
		} else {
			throw new IsAdminException(this, new Throwable().getStackTrace()[0].getMethodName(), network);
		}
	}

	/**
	 * Removes a member from a network where the statement's user is administrator
	 * @param network network's name where remove a member
	 * @param member member to remove
	 * @throws IsAdminException if the member is not admin, throw exception
	 */
	public void removeMember(Network network, Member member) throws IsAdminException {
		if (isAdmin(network)) {
			network.getListMembers().remove(member);
			network.getListSocialClassByMembers().remove(member);
		} else {
			throw new IsAdminException(this, new Throwable().getStackTrace()[0].getMethodName(), network);
		}
	}

	/**
	 * Promotes a member in a network where the statement's user is administrator
	 * @param network network where promote a member
	 * @param member member who will become administrator
	 * @throws IsAdminException if the member is not admin, throw exception
	 * @throws RoleException if the member targeted doesn't have the role required
	 */
	public void promoteAdmin(Network network, Member member) throws IsAdminException, RoleException {
		if (isAdmin(network)) {
			if (network.getListMembers().contains(member)) {
				network.getListAdmins().add(member);
				network.getListMembers().remove(member);
				network.getListSocialClassByMembers().replace(member, SocialClass.REGULAR);
			} else {
				throw new RoleException(this, new Throwable().getStackTrace()[0].getMethodName(), "member", member);
			}
		} else {
			throw new IsAdminException(this, new Throwable().getStackTrace()[0].getMethodName(), network);
		}
	}

	/**
	 * Degrade an admin in the a network where the statement's user is administrator
	 * @param network network where promote a member
	 * @param admin admin who will be degrade to member
	 * @throws IsAdminException if the member is not admin, throw exception
	 * @throws RoleException if the member targeted doesn't have the role required
	 */
	public void degradeAdmin(Network network, Member admin) throws IsAdminException, RoleException {
		if (isAdmin(network)) {
			if (network.getListAdmins().contains(admin)) {
				network.getListAdmins().remove(admin);
				network.getListMembers().add(admin);
			} else {
				throw new RoleException(this, new Throwable().getStackTrace()[0].getMethodName(), "admin", admin);
			}
		} else {
			throw new IsAdminException(this, new Throwable().getStackTrace()[0].getMethodName(), network);
		}
	}

	/**
	 * @param network network where to get the social class of the member
	 * @return member's social class
	 * @throws IsInNetworkException if the member is not in the network, throw exception
	 */
	public SocialClass getSocialClass(Network network) throws IsInNetworkException {
		if (isInNetwork(network)) {
			return network.getListSocialClassByMembers().get(this);
		} else {
			throw new IsInNetworkException(this, new Throwable().getStackTrace()[0].getMethodName(), network);
		}
	}

	/**
	 * @return member's token sum
	 */
	public int getSumTokens() {
		return this.sumTokens;
	}

	public void setSumTokens(int sumTokens) {
		this.sumTokens = sumTokens;
	}

	/**
	 * @return member's name
	 */
	public String getName() {
		return this.name;
	}

	/**
	 * @return member's skill list
	 */
	public ArrayList<Skill> getListSkills() {
		return this.listSkills;
	}
}