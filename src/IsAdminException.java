public class IsAdminException extends Exception {
	private String message;

	public IsAdminException(Member member, String statement, Network network) {
		this.message = "You (" + member.getName() + ") cannot execute the statement '" + statement + "' because you are not an administrator of the network : " + network.getName();
	}

	public String getMessage() {
		return this.message;
	}
}
