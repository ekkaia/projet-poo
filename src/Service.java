import java.util.ArrayList;

public class Service {
	private String name;
	private Member beneficiary;
	private int hourlyCost;
	private int nbMembersRequired;
	private Skill skillRequired;
	private StatusTask statusTask;
	private ArrayList<Member> listWorkers;

	/**
	 * Constructor
	 * @param name service's name
	 * @param beneficiary member who ask for a service
	 * @param hourlyCost hourly cost for doing the service
	 * @param nbMembersRequired number of members required for doing the service
	 * @param skillRequired skill required for doing the service
	 */
	public Service(String name, Member beneficiary, int hourlyCost, int nbMembersRequired, Skill skillRequired) {
		this.name = name;
		this.beneficiary = beneficiary;
		this.hourlyCost = hourlyCost;
		this.nbMembersRequired = nbMembersRequired;
		this.skillRequired = skillRequired;
		listWorkers = new ArrayList<>(nbMembersRequired);
		this.statusTask = StatusTask.WAITFORWORKERS;
	}

	/**
	 * Permits to add a worker to a service
	 * @param worker worker who will be added to the service
	 * @return current number of workers who waits for doing the service
	 * @throws SkillException worker doesn't have the skill required
	 */
	public int addWorker(Member worker) throws SkillException {
		if (listWorkers.size() < nbMembersRequired && worker.getListSkills().contains(skillRequired)) {
			listWorkers.add(worker);
			System.out.println(worker.getName() + " has join '" + name + "' : " + listWorkers.size() + "/" + nbMembersRequired + " workers ready for doing the task!");
		} else {
			throw new SkillException(worker, new Throwable().getStackTrace()[0].getMethodName(), "you don't have the skill required.");
		}

		return listWorkers.size();
	}

	/**
	 * Completes the task: takes a random number between 1-24 which will be the numbers of hours took for did the task.
	 * From that, the cost is calculated and the amount is deducts from the beneficiary's wallet and added to workers wallets.
	 * @param beneficiarySocialClass social class of the beneficiary
	 */
	public void taskComplete(SocialClass beneficiarySocialClass) {
		int min = 1, max = 24;
		int hoursToDoTask =  min + (int)(Math.random() * ((max - min) + 1));
		statusTask = StatusTask.FINISH;

		System.out.println("The task '" + name + "' has been completed in " + hoursToDoTask + " hours.");

		int cost = beneficiarySocialClass.payment(hourlyCost * hoursToDoTask * listWorkers.size());
		subtractTokens(beneficiary, cost);

		System.out.println("\t― " + beneficiary.getName() + " (Social class : " + beneficiarySocialClass + ") has paid " + cost + " tokens for the completion of the task '" + this.getName() + "'.");

		for (int i = 0 ; i < listWorkers.size() ; i++) {
			addTokens(listWorkers.get(i), cost / listWorkers.size());
			System.out.println("\t― " + listWorkers.get(i).getName() + " received " + cost / listWorkers.size() + " tokens for completing the task '" + this.getName() + "'.");
		}
	}

	/**
	 * Subtracts a sum of tokens from the member's wallet
	 * @param tokens sum of tokens to subtract
	 */
	private void subtractTokens(Member member, int tokens) {
		member.setSumTokens(-tokens);
	}

	/**
	 * Add a sum of tokens to the member's wallet
	 * @param tokens sum of tokens to add
	 */
	private void addTokens(Member member, int tokens) {
		member.setSumTokens(tokens);
	}

	/**
	 * @return service's name
	 */
	public String getName() {
		return this.name;
	}

	/**
	 * @return service's beneficiary
	 */
	public Member getBeneficiary() {
		return this.beneficiary;
	}

	/**
	 * @return hourly cost of the service
	 */
	public int getHourlyCost() {
		return this.hourlyCost;
	}

	/**
	 * @return number of members required
	 */
	public int getNbMembersRequired() {
		return this.nbMembersRequired;
	}

	/**
	 * @return skill required for doing the service
	 */
	public Skill getSkillRequired() {
		return this.skillRequired;
	}

	/**
	 * @return status task of the service
	 */
	public StatusTask getStatusTask() {
		return this.statusTask;
	}

	/**
	 * @return list of workers
	 */
	public ArrayList<Member> getListWorkers() {
		return this.listWorkers;
	}
}