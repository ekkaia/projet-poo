public enum SocialClass {
	ZERO,
	HALF,
	REGULAR;

	/**
	 * Calculate the price to pay according to the member's social class
	 * @return price to pay
	 */
	int payment(int price) {
		switch (this) {
			case ZERO:
				return 0;
			case HALF:
				return price / 2;
			case REGULAR:
				return price;
			default:
				throw new AssertionError("The social class of the member doesn't exist : " + this);
		}
	}
}