public class SkillException extends Exception {
	private String message;

	public SkillException(Member member, String statement, String message) {
		this.message = "You (" + member.getName() + ") cannot execute the statement '" + statement + "' because : " + message;
	}

	public String getMessage() {
		return this.message;
	}
}
