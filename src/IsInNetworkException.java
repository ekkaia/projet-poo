public class IsInNetworkException extends Exception {
	private String message;

	public IsInNetworkException(Member member, String statement, Network network) {
		this.message = "You (" + member.getName() + ") cannot execute the statement '" + statement + "' because you are not in the network : " + network.getName();
	}

	public String getMessage() {
		return this.message;
	}
}
