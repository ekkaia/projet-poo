public class Main {
	public static void main(String[] args) throws IsInNetworkException {
		final int sumTokens = 300;

		Member romain = new Member("Romain", sumTokens);
		Member boris = new Member("Boris", sumTokens);
		Member lea = new Member("Léa", sumTokens);
		Member jordan = new Member("Jordan", sumTokens);
		Member mael = new Member("Maël", sumTokens);
		Member antoine = new Member("Antoine", sumTokens);

		Network romainNetwork = romain.createNetwork("Le réseau de Romain");

		try {
			romain.addMember(romainNetwork, lea);
		} catch (IsAdminException e) {
			System.out.println(e.getMessage());
		}

		try {
			romain.addMember(romainNetwork, boris);
		} catch (IsAdminException e) {
			System.out.println(e.getMessage());
		}

		try {
			romain.addMember(romainNetwork, jordan);
		} catch (IsAdminException e) {
			System.out.println(e.getMessage());
		}

		try {
			romain.addMember(romainNetwork, mael);
		} catch (IsAdminException e) {
			System.out.println(e.getMessage());
		}

		try {
			romain.addMember(romainNetwork, antoine);
		} catch (IsAdminException e) {
			System.out.println(e.getMessage());
		}

		try {
			romain.changeSocialClass(romainNetwork, boris, SocialClass.HALF);
		} catch (IsAdminException e) {
			System.out.println(e.getMessage());
		}

		displayNetworkStatus(romainNetwork);

		try {
			romain.promoteAdmin(romainNetwork, antoine);
		} catch (IsAdminException | RoleException e) {
			System.out.println(e.getMessage());
		}

		try {
			mael.degradeAdmin(romainNetwork, romain);
		} catch (IsAdminException | RoleException e) {
			System.out.println(e.getMessage());
		}

		try {
			antoine.changeSocialClass(romainNetwork, jordan, SocialClass.ZERO);
		} catch (IsAdminException e) {
			System.out.println(e.getMessage());
		}

		displayNetworkStatus(romainNetwork);

		try {
			romain.removeMember(romainNetwork, mael);
		} catch (IsAdminException e) {
			System.out.println(e.getMessage());
		}

		Service borisCooking = boris.createService(romainNetwork, "Cooking", 48, 2, Skill.COOKING);

		displayNetworkStatus(romainNetwork);

		try {
			jordan.addSkill(Skill.COOKING);
		} catch (SkillException e) {
			System.out.println(e.getMessage());
		}

		try {
			antoine.addSkill(Skill.COOKING);
			antoine.addSkill(Skill.ELECTRONIC);
		} catch (SkillException e) {
			System.out.println(e.getMessage());
		}

		try {
			boris.addSkill(Skill.COMPUTING);
			boris.addSkill(Skill.GARDENING);
		} catch (SkillException e) {
			System.out.println(e.getMessage());
		}

		displayMemberStatus(jordan);
		displayMemberStatus(boris);
		displayMemberStatus(boris);

		try {
			lea.joinService(romainNetwork, borisCooking);
		} catch (IsInNetworkException | IsServiceExistException | SkillException e) {
			System.out.println(e.getMessage());
		}

		try {
			antoine.joinService(romainNetwork, borisCooking);
		} catch (IsInNetworkException | IsServiceExistException | SkillException e) {
			System.out.println(e.getMessage());
		}

		try {
			jordan.joinService(romainNetwork, borisCooking);
		} catch (IsInNetworkException | IsServiceExistException | SkillException e) {
			System.out.println(e.getMessage());
		}

		try {
			jordan.leaveNetwork(romainNetwork);
		} catch (IsInNetworkException e) {
			System.out.println(e.getMessage());
		}

		ExportCSV exportCSV = new ExportCSV(romainNetwork.getFinishedServices());

		displayNetworkStatus(romainNetwork);
	}

	/**
	 * Display status of the network in the console
	 * @param network network which will be displayed
	 */
	public static void displayNetworkStatus(Network network) {
		System.out.println("Network name : " + network.getName() + "\n\tAdmin (" + network.getListAdmins().size() + ") : ");
		for (int i = 0 ; i < network.getListAdmins().size() ; i++) {
			System.out.println("\t\t― " + network.getListAdmins().get(i).getName() + " (Social class : " + network.getListSocialClassByMembers().get(network.getListAdmins().get(i)) + ")");
		}
		System.out.println("\tMembers (" + network.getListMembers().size() + ") : ");
		for (int i = 0 ; i < network.getListMembers().size() ; i++) {
			System.out.println("\t\t― " + network.getListMembers().get(i).getName() + " (Social class : " + network.getListSocialClassByMembers().get(network.getListMembers().get(i)) + ")");
		}
		System.out.println("\tServices (" + network.getListServices().size() + ") : ");
		for (int i = 0 ; i < network.getListServices().size() ; i++) {
			System.out.println("\t\t― " + network.getListServices().get(i).getName() + " (Created by " + network.getListServices().get(i).getBeneficiary().getName() + ", " + network.getListServices().get(i).getHourlyCost() + " tokens/hours)");
		}
	}

	public static void displayMemberStatus(Member member) {
		System.out.println("Member name : " + member.getName() + "\n\t― Sum tokens : " + member.getSumTokens() + "\n\t― Skills (" + member.getListSkills().size() + ") : ");
		for (int i = 0 ; i < member.getListSkills().size() ; i++) {
			System.out.println("\t\t● " + member.getListSkills().get(i));
		}
	}
}