import java.util.ArrayList;
import java.util.HashMap;

public class Network {
    private String name;
    private ArrayList<Member> listAdmins;
    private ArrayList<Member> listMembers;
    private HashMap<Member, SocialClass> listSocialClassByMembers;
    private ArrayList<Service> listServices;

    /**
     * Constructor
     * @param name name of the network
     * @param member member who created the network and will become his first administrator
     */
    public Network(String name, Member member) {
        this.name = name;
        listAdmins = new ArrayList<>();
        listAdmins.add(member);

        listSocialClassByMembers = new HashMap<>();
        listSocialClassByMembers.put(member, SocialClass.REGULAR);

        listMembers = new ArrayList<>();
        listServices = new ArrayList<>();
    }

    /**
     * Permits to change the member's social class
     * @param member member to change his social class
     * @param socialClass new social class
     */
    public void changeSocialClass(Member member, SocialClass socialClass) {
        getListSocialClassByMembers().replace(member, socialClass);
    }

    /**
     * Adds service to the network's services list
     * @param service service to add
     */
    public void addService(Service service) {
        listServices.add(service);
        System.out.println(service.getBeneficiary().getName() + " has created the task '" + service.getName() + "' (hourly cost : " + service.getHourlyCost() + " ; skill required : " + service.getSkillRequired() + " ; number of workers required : " + service.getNbMembersRequired() + ")");
    }

    /**
     * Removes service from the network's service list
     * @param service service to remove
     */
    public void removeService(Service service) {
    		listServices.remove(service);
    }

    /**
     * Permits to join a service
     * @param worker worker who want to join the service
     * @param service service to join
     * @throws IsInNetworkException if the member is not in the network, throw exception
     * @throws IsServiceExistException if the service doesn't exist, throw exception
     * @throws SkillException if the member doesn't have the skill required, throw exception
     */
    public void joinService(Member worker, Service service) throws IsInNetworkException, IsServiceExistException, SkillException {
        int index = listServices.indexOf(service);
        if (listServices.contains(service)) {
            int nbWorkers = listServices.get(index).addWorker(worker);

            if (nbWorkers == listServices.get(index).getNbMembersRequired()) {
                System.out.println("The task (" + service.getName()  + ") has started!");
                service.taskComplete(listServices.get(index).getBeneficiary().getSocialClass(this));
            }
        } else {
            throw new IsServiceExistException(worker, new Throwable().getStackTrace()[0].getMethodName(), service, this);
        }
    }

    /**
     * @return list of finished services
     */
    public ArrayList<Service> getFinishedServices() {
        ArrayList<Service> listFinishedServices = new ArrayList<>();

        for (Service listService : listServices) {
            if (listService.getStatusTask() == StatusTask.FINISH) {
                listFinishedServices.add(listService);
            }
        }

        return listFinishedServices;
    }

    /**
     * @return network's name
     */
    public String getName() {
        return this.name;
    }

    /**
     * @return member's list of the network
     */
    public ArrayList<Member> getListMembers() {
        return this.listMembers;
    }

    /**
     * @return admin's list of the network
     */
    public ArrayList<Member> getListAdmins() {
        return this.listAdmins;
    }

    /**
     * @return member's list and their social class
     */
    public HashMap<Member, SocialClass> getListSocialClassByMembers() {
        return this.listSocialClassByMembers;
    }

    /**
     * @return network's service list
     */
    public ArrayList<Service> getListServices() {
        return this.listServices;
    }
}