public class RoleException extends Exception {
	private String message;

	public RoleException(Member member, String statement, String role, Member target) {
		this.message = "You (" + member.getName() + ") cannot execute the statement '" + statement + "' on '" + target.getName() + "' because his role is not : " + role;
	}

	public String getMessage() {
		return this.message;
	}
}
