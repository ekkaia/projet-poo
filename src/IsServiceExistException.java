public class IsServiceExistException extends Exception {
	private String message;

	public IsServiceExistException(Member member, String statement, Service service, Network network) {
		this.message = "You (" + member.getName() + ") cannot execute the statement '" + statement + "' because the service '" + service.getName() + "' in the network : " + network.getName();
	}

	public String getMessage() {
		return this.message;
	}
}
